﻿
#  Requisito 21 - Perder o Jogo
## 1 - Resumo
**História de Usuário**
>  Como o jogador, posso perder o jogo.

**Pré-condições**<br />
>1.1. Jogo deverá iniciar com sucesso.<br />
>1.2. O jogador deve estar participando da partida.<br />

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O jogador alcança o topo com as peças do jogo;<br/>

>2. **Fluxo Alternativo** <br/>
	2.1 O jogador desiste do jogo.  <br />
	2.2 O jogador perde conexão com a rede.<br/>    


**Observações**
>A partir do momento que o jogadores desiste ou perde conexão, ele sai da sala de jogo e terá que iniciar o jogo novamente.
## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.


>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **playerlost < iDJogador >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 706c617965726c6f73742031**ff**(Adicionar o caracter 'ff')<br/>
> --------- em **Texto imprimivel**: playerlost 1ÿ<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **playerlost < iDJogador >**<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 706c617965726c6f73742031<br/>
> --------- em **Texto imprimivel**: playerlost 1<br/>
## 3 - Diagrama de sequência

![Requisito 21](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%2021/Requisitos%2021%20-%20Perder%20o%20Jogo.png)



